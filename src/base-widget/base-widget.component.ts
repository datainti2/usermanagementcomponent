import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import * as _ from 'underscore';

declare var jQuery:any;
declare var carousel:any;
@Component({
  selector: 'app-base-widget',
  template: '<div class="container"><div class="col-md-12"><div  class="content catalog-group" *ngFor="let item of data"><div  class="list-group-item"><div  class="row"><div  class="col-md-8 col-sm-8 col-lg-8" style="float: left;"><div  class="row"><div  class="col-md-6 col-sm-6 col-lg-6">{{item.name}}</div><div  class="col-md-3 col-sm-3 col-lg-3">{{item.role_name}}</div><div  class="col-md-3 col-sm-3 col-lg-3"><span *ngIf="item.status == '+'Active'+'"  style="color: limegreen"> {{item.status}}</span><span *ngIf="item.status == '+'Inactive'+'"  style="color: darkred"> {{item.status}}</span></div></div></div><div  class="col-md-2 col-sm-2 col-lg-2"><button  class="btn btn-sm btn-outline-primary" style="float: right;" type="button" (click) = "editData(item.id)">Edit</button></div><div  class="col-md-2 col-sm-2 col-lg-2"><button  class="btn btn-sm btn-outline-danger" style="float: right;" type="button" (click) = "deleteData(item.id)">X</button></div></div></div></div><div class="col-md-12 "><div class="col-md-2 "><span class="pagination">{{data.length}} Users</span></div><div class="col-md-10 "> <ul *ngIf="pager.pages && pager.pages.length" class="pagination pull-right "><li [ngClass]="{disabled:pager.currentPage === 1}"><a (click)="setPage(pager.currentPage - 1)">Prev</a></li><li [ngClass]="{disabled:pager.currentPage === pager.totalPages}"><a (click)="setPage(pager.currentPage + 1)">Next</a></li></ul></div> </div></div></div>',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {
// pager object


public data: Array<any> = [
 {
  "id": 1,
  "name": "rina2",
  "role_name": "analyst",
  "status":"Active",
 
 },
 {
  "id": 2,
  "name": "rina3",
  "role_name": "analyst",
  "status":"Inactive",
 
 },
 
 ];
  pager: any = {};

  // paged items
  pagedItems: any[];

  constructor(private http: Http) { }

  ngOnInit() {
    this.data;
     this.setPage(1);

  }

  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 8) {
            // calculate total pages
            let totalPages = Math.ceil(totalItems / pageSize);

            let startPage: number, endPage: number;
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }

            // calculate start and end item indexes
            let startIndex = (currentPage - 1) * pageSize;
            let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

            // create an array of pages to ng-repeat in the pager control
            let pages = _.range(startPage, endPage + 1);

            // return object with all pager properties required by the view
            return {
                totalItems: totalItems,
                currentPage: currentPage,
                pageSize: pageSize,
                totalPages: totalPages,
                startPage: startPage,
                endPage: endPage,
                startIndex: startIndex,
                endIndex: endIndex,
                pages: pages
            };
        }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service
        this.pager = this.getPager(this.data.length, page);

        // get current page of items
        this.pagedItems = this.data.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    editData (id){
      alert("id " + id);
    }

    deleteData (id){
      // console.log(id);
      alert("id " + id);
    }

}
